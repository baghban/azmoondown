MathJax.Hub.Config({
    jax: ["input/TeX", "output/HTML-CSS"],
    tex2jax: {
        inlineMath: [ ['$', '$'] ],
        displayMath: [ ['$$', '$$']],
        processEscapes: true,
        skipTags: ['script', 'noscript', 'style', 'textarea', 'pre', 'code']
    },
    messageStyle: "none",
    "HTML-CSS": { 
        preferredFont: "TeX", 
        availableFonts: ["STIX","TeX"]
    }
});

window.matchMedia("print").addListener(function(e) {
    if (e.matches) {
        console.log("the view port is print");
        paging();
    }
});

window.matchMedia("screen").addListener(function(e) {
    if (e.matches) {
        console.log("the view port is screen");
        //						paging();
    }
});
String.prototype.toFaDigit = function() {
    return this.replace(/\d+/g, function(digit) {
        var ret = '';
        for (var i = 0, len = digit.length; i < len; i++) {
            ret += String.fromCharCode(digit.charCodeAt(i) + 1728);
        }
        return ret;
    });
};

function toFaDigit() {
    var i;
    decimals = document.querySelectorAll(".MJXc-TeX-main-R, .point");
    for (i = 0; i < decimals.length; i++) {
        decimals[i].innerHTML = decimals[i].innerHTML.replace(/\.(?=\d+)/g, "٫");
    }

    var nodes = document.querySelectorAll('#rawAzmoon *:not(script):not(boom)')
    for (i = 0; i < nodes.length; i++) {
        if (nodes[i].children.length == 0) {
            var txt = nodes[i].innerText;
            if (txt) {
                nodes[i].innerText = txt.toFaDigit();
            }
        }
    }
}

function paging(rawAzmoonId="rawAzmoon",wrapPagesId="pages") {
    // var headers = document.getElementById("headers");
    // var headers = headers.querySelectorAll(".header");
    // var header = headers[0];
    var rawAzmoon = document.getElementById(rawAzmoonId);
    var questions = rawAzmoon.querySelectorAll(".question-group");
    var pages = document.querySelectorAll(".page");

    var i;
    for (i = 0; i < pages.length; i++) {
        pages[i].remove();
    }

    var totalPage = 0;
    var curPage = 0;
    var wrapPages = document.getElementById(wrapPagesId);
    var tmpPage = document.createElement("div");
    tmpPage.className = "tmp-page";
    wrapPages.appendChild(tmpPage);

    var refPage = document.createElement("div");
    refPage.className = "page";
    wrapPages.appendChild(refPage);
    // if (header) {
    //     var headerCopy = header.cloneNode(true);
    //     tmpPage.appendChild(headerCopy);
    // }
    for (i = 0; i < questions.length; i++) {
        var question = questions[i];
        quesCopy = question.cloneNode(true);

        var number = quesCopy.querySelector(".number");
        number.innerHTML = (i + 1).toString().toFaDigit();
        
        tmpPage.appendChild(quesCopy);

        var blankes = quesCopy.querySelectorAll(".blank");
        var j;
        for(j = 0; j < blankes.length; j++) {
            var dot = blankes[j].previousElementSibling;
            var widthBlank = blankes[j].offsetWidth;

            dot.innerHTML = "ـ";
            while (dot.offsetWidth < widthBlank + 10) {
                dot.innerHTML += " ـ";
            }

            var widthDot = dot.offsetWidth;
            var diffWidthes = widthDot - widthBlank;
            blankes[j].style.marginRight = -(widthBlank + diffWidthes / 2) + "px";
            blankes[j].style.marginLeft = diffWidthes / 2 + "px";
        }

        var booms = quesCopy.querySelectorAll("boom")
        for (j=0; j < booms.length; j++){
            var boomDiv = document.createElement('div');
            boomDiv.className = 'boom';
            boomDiv.id = 'boom' + '-' + i + '-' + j;
            var newScript = document.createElement('script');
            var inlineScript = document.createTextNode(booms[j].innerText);
            newScript.appendChild(inlineScript);
            booms[j].after(boomDiv);
            inlineScript = document.createTextNode(`
                boomDiv = document.getElementById("boom-${i}-${j}");
                boomDiv.appendChild(boom.getElem());
                `);
            newScript.appendChild(inlineScript);
            boomDiv.after(newScript);
            booms[j].remove();
        }

        if (tmpPage.offsetHeight > refPage.offsetHeight) {
            quesCopy.remove();

            curPage += 1;
            totalPage += 1;
            var page = document.createElement("div");
            //page.id = "page-p-" + curPage;
            page.className = "page";
            wrapPages.appendChild(page);
            page.innerHTML = tmpPage.innerHTML;

            tmpPage.innerHTML = "";
            refPage.innerHTML = "";


            tmpPage.appendChild(quesCopy)
        }
    }
    curPage += 1;
    totalPage += 1;
    var page = document.createElement("div");
    //page.id = "page-p-" + curPage;
    page.className = "page";
    wrapPages.appendChild(page);
    page.innerHTML = tmpPage.innerHTML;

    tmpPage.remove();
    refPage.remove();
}

function render() {
    toFaDigit();
    paging();
}
