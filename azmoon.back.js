MathJax.Hub.Config({
    jax: ["input/TeX", "output/HTML-CSS"],
    tex2jax: {
        inlineMath: [ ['$', '$'] ],
        displayMath: [ ['$$', '$$']],
        processEscapes: true,
        skipTags: ['script', 'noscript', 'style', 'textarea', 'pre', 'code']
    },
    messageStyle: "none",
    "HTML-CSS": { 
        preferredFont: "TeX", 
        availableFonts: ["STIX","TeX"]
    }
});
MathJax.Hub.Queue(function() {

    // size = document.getElementsByTagName("BODY")[0].getAttribute("size");
    // paper = new Paper(size);

    var nodes = document.querySelectorAll('#questions *')
    var i;
    for (i = 0; i < nodes.length; i++) {
        if (nodes[i].children.length == 0) {
            var txt = nodes[i].innerText;
            if (txt) {
                nodes[i].innerText = txt.toFaDigit();
            }
        }
    }

    decimals = document.querySelectorAll(".MJXc-TeX-main-R, .point");
    var i;
    for (i = 0; i < decimals.length; i++) {
        decimals[i].innerHTML = decimals[i].innerHTML.replace(/\./g, "٫");
    }

    locateCheckboxes();

    questions = document.querySelectorAll(".question-group");
    var i;
    for (i = 0; i < questions.length; i++) {
        var numQues = document.createElement("div");
        numQues.className = "number";
        numQues.innerHTML = (i + 1).toString().toFaDigit();
        questions[i].insertBefore(numQues, questions[i].firstChild);
    }

    // mathtext = document.querySelectorAll('.mjx-char');
    // var i;
    // for (i = 0; i < mathtext.length; i++) {
    //     mathtext[i].innerText = mathtext[i].innerText;
    // }

    render();
});

window.matchMedia("print").addListener(function(e) {
    if (e.matches) {
        console.log("the view port is print");
        render();
    }
});

window.matchMedia("screen").addListener(function(e) {
    if (e.matches) {
        console.log("the view port is screen");
        //						render();
    }
});
String.prototype.toFaDigit = function() {
    return this.replace(/\d+/g, function(digit) {
        var ret = '';
        for (var i = 0, len = digit.length; i < len; i++) {
            ret += String.fromCharCode(digit.charCodeAt(i) + 1728);
        }
        return ret;
    });
};

function render(wrapPage="BODY") {
    var headers = document.getElementById("headers");
    var headers = headers.querySelectorAll(".header");
    var header = headers[0];
    var questions = document.getElementById("questions");
    var questions = questions.querySelectorAll(".question-group");
    var pages = document.querySelectorAll(".page");

    var i;
    for (i = 0; i < pages.length; i++) {
        pages[i].remove();
    }

    var totalPage = 0;
    var curPage = 0;
    var body = document.getElementsByTagName("BODY")[0];
    var tmpPage = document.createElement("div");
    tmpPage.className = "tmp-page";
    body.appendChild(tmpPage);

    var refPage = document.createElement("div");
    refPage.className = "page";
    body.appendChild(refPage);
    if (header) {
        var headerCopy = header.cloneNode(true);
        tmpPage.appendChild(headerCopy);
    }
    for (i = 0; i < questions.length; i++) {
        var question = questions[i];
        quesCopy = question.cloneNode(true);
        tmpPage.appendChild(quesCopy);
        if (tmpPage.offsetHeight > refPage.offsetHeight) {
            quesCopy.remove();

            curPage += 1;
            totalPage += 1;
            var page = document.createElement("div");
            //page.id = "page-p-" + curPage;
            page.className = "page";
            body.appendChild(page);
            page.innerHTML = tmpPage.innerHTML;

            tmpPage.innerHTML = "";
            refPage.innerHTML = "";


            tmpPage.appendChild(quesCopy)
        }
    }
    curPage += 1;
    totalPage += 1;
    var page = document.createElement("div");
    //page.id = "page-p-" + curPage;
    page.className = "page";
    body.appendChild(page);
    page.innerHTML = tmpPage.innerHTML;

    tmpPage.remove();
    refPage.remove();
}

function locateCheckboxes() {
    checkbox = document.getElementById("checkbox");
    checkbox = checkbox.content.cloneNode(true);
    checkboxes = document.querySelectorAll(".prop:not(.true):not(.false) .checkbox, .choice:not(.true) .checkbox");
    var i;
    for(i=0; i < checkboxes.length; i++) {
        var clone = checkbox.cloneNode(true);
        checkboxes[i].appendChild(clone);
    }

    checkboxTick = document.getElementById("checkbox-tick");
    checkboxTick = checkboxTick.content.cloneNode(true);
    checkboxes = document.querySelectorAll(".prop.true .checkbox, .choice.true .checkbox");
    var i;
    for(i=0; i < checkboxes.length; i++) {
        var clone = checkboxTick.cloneNode(true);
        checkboxes[i].appendChild(clone);
    }

    checkboxX = document.getElementById("checkbox-x");
    checkboxX = checkboxX.content.cloneNode(true);
    checkboxes = document.querySelectorAll(".prop.false .checkbox");
    var i;
    for(i=0; i < checkboxes.length; i++) {
        var clone = checkboxX.cloneNode(true);
        checkboxes[i].appendChild(clone);
    }
}