class Base {
    constructor() {
        this.attributes = {};
        this._elem = null;
    }

    attrs(attrs) {
        for (const attr in attrs) {
            // this.attributes[attr] = attrs[attr];
            this.getElem().setAttribute(attr, attrs[attr]);
        }
        return this;
    }

    getElem() {
        return this._elem;
    }

    // renderAttrs() {
    //     var attrString = '';
    //     for (const attr in this.attributes) {
    //         attrString += attr + '="' + this.attributes[attr] + '" '
    //     }
    //     return attrString;
    // }
}

class Boom extends Base {
    constructor(dimension=[8,4], strokeWidth=0.025, color="black", fill="black", unit = 40) {
        super();
        this.autoDim = false;
        this.unit = unit;
        this._elem = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        this._childs = {};
        this.width = dimension[0];
        this.height = dimension[1];
        this.attrs({
            'width': this.width * this.unit,
            'height': this.height * this.unit,
            'stroke': color,
            'stroke-width': strokeWidth * this.unit,
            'fill': fill
        });
    }

    getChild(label) {
        return this._childs[label];
    }

    addChild(label, child) {
        this._childs[label] = child;
        if(child.getElem()){
            this._elem.appendChild(child.getElem());
        }
        return this;
    }

    svgCoordinate(x,y){
        if(x instanceof Point) {
            return [x.x * this.unit, (this.height - x.y) * this.unit];
        }
        if(typeof y === "number") {
            return [x * this.unit, (this.height - y) * this.unit];
        } else {
            return x * this.unit;
        }
    }

    point(label, x, ...rest) {
        var type = 'cartesiane';
        var relPoint = {x: 0, y: 0};
        var y;
        if(Array.isArray(x)){
            y = x[1];
            x = x[0];
        } else {
            y = rest[0];
            rest.shift();
        }
        var i;
        for(i=0; i<rest.length; i++) {
            if(rest[i] === 'polar' || rest[i] === 'cartesiane') {
                type = rest[i];
            } else {
                var elem = this.getChild(rest[i]);
                if(elem && elem instanceof Point) {
                    relPoint = elem;
                }
            }
        }

        var point = new Point(this, x, y, type, relPoint);
        this.addChild(label, point);
        return this;
    }

    cpoint(label, x, ...rest) {
        var type = 'cartesiane';
        var r = 0.05;
        var relPoint = {x: 0, y: 0};
        var y;
        if(Array.isArray(x)){
            y = x[1];
            x = x[0];
        } else {
            y = rest[0];
            rest.shift();
        }
        var i;
        for(i=0; i<rest.length; i++) {
            if(typeof rest[i] === "string"){
                if(rest[i] === 'polar' || rest[i] === 'cartesiane') {
                    type = rest[i];
                } else {
                    var elem = this.getChild(rest[i]);
                    if(elem && elem instanceof Point) {
                        relPoint = elem;
                    }
                }
            } else if(typeof rest[i] === "number"){
                r = rest[i];
            }
        }

        var cpoint = new CPoint(this, x, y, type, r, relPoint);
        this.addChild(label, cpoint);
        return this;
    }

    segment(label, x1, y1, x2, y2) {
        var y1org = y1;
        if(arguments.length === 3) {
            if(typeof x1 === "string") {
                var elem = this.getChild(x1);
                if(elem && elem instanceof Point) {
                    x1 = elem.x;
                    y1 = elem.y;
                }
            } else if(x1 instanceof Point) {
                y1 = x1.y;
                x1 = x1.x;
            } else if(Array.isArray(x1)) {
                y1 = x1[1];
                x1 = x1[0]
            }

            if(typeof y1org === "string") {
                var elem = this.getChild(y1org);
                if(elem && elem instanceof Point) {
                    x2 = elem.x;
                    y2 = elem.y;
                }
            } else if(y1org instanceof Point) {
                y2 = y1org.y;
                x2 = y1org.x;
            } else if(Array.isArray(y1org)) {
                y2 = y1org[1];
                x2 = y1org[0]
            }
        }
        if(arguments.length === 4) {
            if(typeof x1 === "string") {
                var elem = this.getChild(x1);
                if(elem && elem instanceof Point) {
                    x1 = elem.x;
                    y1 = elem.y;
                }
            } else if(x1 instanceof Point) {
                y1 = x1.y;
                x1 = x1.x;
            } else if(Array.isArray(x1)) {
                y1 = x1[1];
                x1 = x1[0]
            }
            if(typeof x2 === "string") {
                var elem = this.getChild(x2);
                if(elem && elem instanceof Point) {
                    x2 = elem.x;
                    y2 = elem.y;
                }
            } else if(x2 instanceof Point) {
                y2 = x2.y;
                x2 = x2.x;
            } else if(Array.isArray(x2)) {
                y2 = x2[1];
                x2 = x2[0]
            }
        }
        var seg = new Segment(this, x1, y1, x2, y2);
        this.addChild(label, seg);
        return this;
    }

    intersect(label, elm1, elm2) {
        elm1 = this.getChild(elm1);
        elm2  =this.getChild(elm2);
        if(elm1 instanceof Segment && elm2 instanceof Segment) {
            var m1 = elm1.m();
            var m2 = elm2.m();
            if(m1 === m2) {return null;}
            var x, y;
            var b1 = elm1.b();
            var b2 = elm2.b();
            x = (b2 - b1) / (m1 - m2);
            y = m1 * x + b1;
            this.addChild(label, new Point(this, x, y));
            return this;
        }
    }

    markAngle(seg1, seg2, sector, length = .2) {
        var seg1Elem = this.getChild(seg1);
        var seg2Elem = this.getChild(seg2);
        var point = seg1Elem.intersect(seg2Elem);
        if(point) {
            var minDeg = seg1Elem.angle();
            var maxDeg = seg2Elem.angle();
            if(maxDeg < minDeg) {
                [minDeg, maxDeg] = [maxDeg, minDeg];
            }
            var startDeg, endDeg, largeArcFlag;
            if(sector < 0) {
                sector = -sector;
                largeArcFlag = 1;
            }
            switch (sector) {
                case 1:
                    startDeg = minDeg;
                    endDeg = maxDeg;
                    break;
                case 2:
                    startDeg = maxDeg;
                    endDeg = 180 + minDeg;
                    break;
                case 3:
                    startDeg = 180 + minDeg;
                    endDeg = 180 + maxDeg;
                    break;
                case 4:
                    startDeg = -180 + maxDeg;
                    endDeg = minDeg;
                default:
                    break;
            }
            if(largeArcFlag === 1) {[startDeg, endDeg] = [endDeg, startDeg];}
            var arc  = new Arc(this, point, length, startDeg, endDeg);
            this.addChild('angle'+seg1+'m'+seg2+'.'+sector,arc);
            return this;
        }
    }

    markRightAngle(seg1, seg2, sector, length=0.2) {
        var seg1Elem = this.getChild(seg1);
        var seg2Elem = this.getChild(seg2);
        var point = seg1Elem.intersect(seg2Elem);
        if(point) {
            var minDeg = seg1Elem.angle();
            var maxDeg = seg2Elem.angle();
            if(maxDeg < minDeg) {
                [minDeg, maxDeg] = [maxDeg, minDeg];
            }
            var startDeg, endDeg, largeArcFlag;
            switch (sector) {
                case 1:
                    startDeg = minDeg;
                    endDeg = maxDeg;
                    break;
                case 2:
                    startDeg = maxDeg;
                    endDeg = 180 + minDeg;
                    break;
                case 3:
                    startDeg = 180 + minDeg;
                    endDeg = 180 + maxDeg;
                    break;
                case 4:
                    startDeg = -180 + maxDeg;
                    endDeg = minDeg;
                default:
                    break;
            }
            var pt1 = new Point(this, startDeg, length, 'polar', point);
            var pt2 = new Point(this, endDeg, length, 'polar', pt1);
            var pt3 = new Point(this, endDeg, length, 'polar', point);
            var polyline  = new Polyline(this, pt1.x,pt1.y,pt2.x,pt2.y,pt3.x,pt3.y);
            this.addChild('rightangle'+seg1+'m'+seg2+'.'+sector,polyline);
            return this;
        }
    }
    
    polygon(label, ...points) {
        var pts = [];
        var i;
        for(i=0; i<points.length; i++) {
            if(typeof points[i] === 'number') {
                pts.push(points[i]);
                pts.push(points[i+1]);
                i++;
            } else if(typeof points[i] === 'string') {
                var pt = this.getChild(points[i]);
                pts.push(pt.x);
                pts.push(pt.y);
            }
        }
        this.addChild(label, new Polygon(this,...pts));
        return this;
    }

    polyline(label, ...points) {
        var pts = [];
        var i;
        for(i=0; i<points.length; i++) {
            if(typeof points[i] === 'number') {
                pts.push(points[i]);
                pts.push(points[i+1]);
                i++;
            } else if(typeof points[i] === 'string') {
                var pt = this.getChild(points[i]);
                pts.push(pt.x);
                pts.push(pt.y);
            }
        }
        this.addChild(label, new Polyline(this,...pts));
        return this;
    }

    // render() {
    //     var res = '<svg ' + this.renderAttrs() + ' ><g>';
    //     for(const elm in this._childs) {
    //         res += this._childs[elm].render();
    //     }
    //     res += '</g></svg>';
    //     return res;
    // }
}

class Point extends Base{
    constructor(boom, x, y, type='cartesiane', relPoint={x: 0, y:0}) {
        super();
        // [x, y] = boom.svgCoordinate(x,y);
        this.x = x;
        this.y = y;
        if (type == 'polar') {
            var deg = Math.PI * x / 180;
            this.x = y * Math.cos(deg);
            this.y = y * Math.sin(deg);
        }
        this.x += relPoint.x;
        this.y += relPoint.y;
    }

    getElem() {
        return null;
    }
    // render() {
    //     return '';
    // }
}

class CPoint extends Point {
    constructor(boom, x, y, type='cartesiane', r=0, relPoint={x: 0, y:0}) {
        super(boom, x, y, type, relPoint);
        this.r = r;
        this._elem = document.createElementNS('http://www.w3.org/2000/svg', 'circle');

        var xsvg, ysvg, rsvg;
        [xsvg, ysvg] = boom.svgCoordinate(this.x, this.y);
        rsvg = boom.svgCoordinate(this.r)
        this.attrs({
            'cx': xsvg,
            'cy': ysvg,
            'r': rsvg
        })

    }

    // render(){
    //     return `<circle ${this.renderAttrs()} />`
    // }
}

class Segment extends Base {
    constructor(boom, x1, y1, x2, y2) {
        super();
        this._elem = document.createElementNS('http://www.w3.org/2000/svg', 'line');
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        var x1svg, x2svg, y1svg, y2svg;
        [x1svg, y1svg] = boom.svgCoordinate(x1, y1);
        [x2svg, y2svg] = boom.svgCoordinate(x2, y2);
        this.attrs({
            'x1': x1svg,
            'y1': y1svg,
            'x2': x2svg,
            'y2': y2svg,
        })
    }

    // render() {
    //     return '<line ' + this.renderAttrs() + ' />';
    // }

    m() {
        if(this.x1 == this.x2) return Infinity;
        return (this.y2 - this.y1) / (this.x2 - this.x1);
    }

    b() {
        var m = this.m();
        if (m === Infinity) {return NaN;}
        return this.y1 - m * this.x1;
    }

    angle() {
        var m = this.m();
        if(m === Infinity) {return 90;}
        var radAngle = Math.atan(m);
        var degAngle = 180 * radAngle / Math.PI;
        if(degAngle < 0){degAngle += 180;}
        return degAngle;
    }

    intersect(elem) {
        if(elem instanceof Segment) {
            var m1 = this.m();
            var m2 = elem.m();
            if(m1 === m2) {return null;}
            var x, y;
            var b1 = this.b();
            var b2 = elem.b();
            if(m1 === Infinity) {return new Point(this,this.x1, m2 * this.x1 + b2);}
            if(m2 === Infinity) {return new Point(this,elem.x1, m1 * elem.x1 + b1);}
            x = (b2 - b1) / (m1 - m2);
            y = m1 * x + b1;
            return new Point(this, x, y);
        }
    }

    angleBetween(seg) {
        if(seg instanceof Segment) {
            var m1 = this.m();
            var m2 = line.m();
            if(m1 * m2 == -1) {return 90;}
            var radAngle = Math.atan(Math.abs((m1 - m2) / (1 + m1 * m2)));
            var degAngle = 180 * radAngle / Math.PI;
            if(degAngle < 0){degAngle += 180;}
            return degAngle;
        }
    }
}

class Arc extends Base {
    constructor(boom, c, r, bd, ed) {
        super();
        this._elem = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        this.boom = boom;
        this.center = c;
        this.radius = r;
        this.beginDegree = bd;
        this.endDegree = ed;
        this.beginPoint = new Point(boom, bd, r, 'polar', c);
        this.endPoint = new Point(boom, ed, r, 'polar', c);
        // this.attributes['d'] = this.path();
        // this.attributes['fill'] = 'none';
        this.attrs({
            'd': this.path(),
            'fill': 'none'
        })
    }

    path() {
        var largeArcFlag;
        var beginpointxsvg, beginpointysvg, endpointxsvg, endpointysvg, radiussvg;
        [beginpointxsvg, beginpointysvg] = this.boom.svgCoordinate(this.beginPoint);
        [endpointxsvg, endpointysvg] = this.boom.svgCoordinate(this.endPoint);
        radiussvg = this.boom.svgCoordinate(this.radius);
        largeArcFlag = (this.beginDegree < this.endDegree) ? 0 : 1;
        return 'M ' + beginpointxsvg + ' ' + beginpointysvg + ' A ' + radiussvg + ' ' + radiussvg + ' 0 ' + largeArcFlag + ' 0 ' + endpointxsvg + ' ' + endpointysvg;
    }

    // render() {
    //     return '<path ' + this.renderAttrs() + ' />'
    // }
}

class Text extends Base {
    constructor(x, y, text) {
        super();
        if(arguments.length === 2) {
            text = y;
            y = x.y;
            x = x.x;
        }
        this.x = x;
        this.y = y;
        this.text = text;
        this.attributes['x'] = x;
        this.attributes['y'] = y;
    }

    render() {
        return '<text ' + this.renderAttrs() + ' >' + this.text + '</text>'
    }
}

class Grid {
    constructor(x1,y1,x2,y2,step='1',color='blue') {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.color = color;
    }
    render() {
        grid = '<g stroke="' + this.color +'">';
        var i;
        for (i=this.x1; i<= this.x2; i++) {
            var line = new Line(i, this.y1, i, this.y2);
            grid += line.render();
        }
        grid += '</g>';
        return grid;
    }
}

class Polygon extends Base {
    constructor(boom, ...points) {
        super();
        this._elem = document.createElementNS('http://www.w3.org/2000/svg', 'polygon');
        this.attrs({'fill': 'none'});
        this.points = [];
        var pointsAttr = '';
        var i;
        for(i=0; i<points.length; i+=2){
            this.points[i] = new Point(boom, points[i], points[i+1]);
            var svgPointX, svgPointY;
            [svgPointX, svgPointY] = boom.svgCoordinate(points[i], points[i+1]);
            pointsAttr += svgPointX + ',' + svgPointY + ' ';
        }
        this.attrs({'points': pointsAttr});
    }

    // render() {
    //     return '<polygon ' + this.renderAttrs() + ' />';
    // }
}

class Polyline extends Base {
    constructor(boom, ...points) {
        super();
        this._elem = document.createElementNS('http://www.w3.org/2000/svg', 'polyline');
        this.attrs({'fill': 'none'});
        this.points = [];
        var pointsAttr = '';
        var i;
        for(i=0; i<points.length; i+=2){
            this.points[i] = new Point(boom, points[i], points[i+1]);
            var svgPointX, svgPointY;
            [svgPointX, svgPointY] = boom.svgCoordinate(points[i], points[i+1]);
            pointsAttr += svgPointX + ',' + svgPointY + ' ';
        }
        this.attrs({'points': pointsAttr});
    }

    // render() {
    //     return '<polyline ' + this.renderAttrs() + ' />';
    // }
}

