(function (extension) {
    if (typeof showdown !== 'undefined') {
        // global (browser or nodejs global)
        extension(showdown);
    } else if (typeof define === 'function' && define.amd) {
        // AMD
        define(['showdown'], extension);
    } else if (typeof exports === 'object') {
        // Node, CommonJS-like
        module.exports = extension(require('showdown'));
    } else {
        // showdown was not found so we throw
        throw Error('Could not find showdown library');
    }
}
    (function (showdown) {
        const alefba = "آبپتثجچحخدذرزژسشصضطظعغفقکگلمنوهی";
        const abjad = "آبجدهوزحطیکلمنسعفصقرشتثخذضظغ";
        const alefbaAlef = "الفبپتثجچحخدذرزژسشصضطظعغفقکگلمنوهی";
        const abjadAlef = "الفبجدهوزحطیکلمنسعفصقرشتخذضظغ";
        const alphabet = "abcdefghijklmnopqrstucwxyz";

        const tags = {
            tick: {
                persian: '\\:تیک\\:',
                english: '\\:tick\\:'
            },
            cross: {
                persian: '\\:ضربدر\\:',
                english: '\\:cross\\:'
            },
            hfill: {
                persian: '>>',
                english: '>>'
            },
            question : {
                persian: '؟',
                english: '\\?'
            },
            parts : {
                persian: 'چب',
                english: 'mp'
            },
            part: {
                persian: '\\)',
                english: '\\)'
            },
            props: {
                persian: 'دن',
                englesh: 'tf'
            },
            prop: {
                persian: '(د|ن)?\\)',
                english: '(t|f)?\\)'
            },
            choices: {
                persian: 'چگ',
                englesh: 'mc'
            },
            choice: {
                persian: '(د|ن)?\\)',
                english: '(t|f)?\\)'
            },
            boom: {
                persian: '<boom>',
                englesh: '<boom>'
            },
            // boomend: {
            //     persian: '<boom>',
            //     englesh: '<boom>'
            // },
            cols: {
                persian: '([1-6])\\ ?ستونی?',
                english: '([1-6])\\ ?cols?'
            }
        }


        const defaultLanguage = 'persian';
        const defaultPartLabel = 'alefba';
        const defaultPropLabel = 'alefba';
        const defaultChoiceLabel = 'abjad';
        const defaultPartCols = 1;
        const defaultPropCols = 1;
        const defaultChoiceCols = 4;

        const checkbox = 
            '<svg viewBox="-11 -12 28 23">'+
                '<circle style="opacity:1;fill:none;stroke:#000000;stroke-width:1.2;stroke-opacity:1" cx="0" cy="0" r="10"/>'+
                '<path class="tick" transform="translate(-6,-2)" style="fill:var(--answer-color)" d="M 1.4142136 0 Q -0.15 -0.15 0 1.4142136 l 4.9497475 4.9497475 q 1.4142136 1.4142136 2.8284271 0 l 14.849242 -14.849242 q 0 -1.4142136 -1.4142136 -1.4142136 l -14.849242 14.849242 Z"/>'+
                '<g class="cross" style="fill:var(--answer-color);stroke-width:1">'+
                    '<path d="M 5.6568542 4.2426407 Q 5.8 5.8 4.2426407 5.6568542 L -5.6568542 -4.2426407 Q -5.8 -5.8 -4.2426407 -5.6568542 Z"/>'+
                    '<path transform="scale(1, -1)" d="M 5.6568542 4.2426407 Q 5.8 5.8 4.2426407 5.6568542 L -5.6568542 -4.2426407 Q -5.8 -5.8 -4.2426407 -5.6568542 Z"/>'+
                '</g>'+
            '</svg>';

        const tick = 
            '<svg viewBox="0 -10 23 18">'+
                '<path d="M 1.4142136 0 Q -0.15 -0.15 0 1.4142136 l 4.9497475 4.9497475 q 1.4142136 1.4142136 2.8284271 0 l 14.849242 -14.849242 q 0 -1.4142136 -1.4142136 -1.4142136 l -14.849242 14.849242 Z"/>'+
            '</svg>';

        const cross = 
            '<svg viewBox="-6 -6 23 18">'+
                '<g>'+
                    '<path d="M 5.6568542 4.2426407 Q 5.8 5.8 4.2426407 5.6568542 L -5.6568542 -4.2426407 Q -5.8 -5.8 -4.2426407 -5.6568542 Z"/>'+
                    '<path transform="scale &#40; 1, -1 &#41;" d="M 5.6568542 4.2426407 Q 5.8 5.8 4.2426407 5.6568542 L -5.6568542 -4.2426407 Q -5.8 -5.8 -4.2426407 -5.6568542 Z"/>'+
            '   </g>'+
            '</svg>';

            String.prototype.toEnDigit = function() {
                return this.replace(/[۱۲۳۴۵۶۷۸۹۰]+/g, function(digit) {
                    var ret = '';
                    for (var i = 0, len = digit.length; i < len; i++) {
                        ret += String.fromCharCode(digit.charCodeAt(i) - 1728);
                    }
                    return ret;
                });
            };

        function labelChar(type, pos, options){
            var number;
            switch (type) {
                case 'part':
                    label = options.partLabel ? options.partLabel : defaultPartLabel;
                    break;
                case 'prop':
                    label = options.propLabel ? options.propLabel : defaultPropLabel;
                    break;
                case 'choice':
                    label = options.choiceLabel ? options.choiceLabel : defaultChoiceLabel;
                    break;
                default:
                    label = 'alefba'
                    break;
            }
            switch (label) {
                case 'alefba':
                    number = pos % 32;
                    return alefba.charAt(number)
                    break;
                case 'abjad':
                    number = pos % 28;
                    return abjad.charAt(number)
                    break;
                case 'alefbaalef':
                    number = pos % 32 - 2;
                    number = (number == 0) ? number : number - 2;
                    return alefbaAlef.charAt(number)
                    break;
                case 'abjadalef':
                    number = pos % 28 - 2;
                    number = (number == 0) ? number : number - 2;
                    return abjadAlef.charAt(number)
                    break;
            }
        }

        function renderStyle(style) {
            var result = 'style="';
            var rule;
            for(rule in style) {
                result += rule + ': ' + style[rule] + ';';
            }
            result += '"';
            return result;
        }


        var azmoon = function() {
            return [
                // tick
                {
                    type: 'lang',
                    filter: function (text, converter, options) {
                        var language = options.language ? options.language : defaultLanguage;
                        tagtick = tags.tick[language]
                        var reg = new RegExp(tagtick,'g');
                        var result = text.replace(reg, '<span class="tick">' + tick + '</span>');
                        return result;
                    }
                },

                // cross
                {
                    type: 'lang',
                    filter: function (text, converter, options) {
                        var language = options.language ? options.language : defaultLanguage;
                        tagcross = tags.cross[language]
                        var reg = new RegExp(tagcross,'g');
                        var result = text.replace(reg, '<span class="cross">' + cross + '</span>');
                        return result;
                    }
                },

                //bmatrix
                {
                    type: 'lang',
                    filter: function (text, converter, options) {
                        var reg = /(¨D.*?)\[(.*?\\\\.*?)\](.*?¨D)/gm;
                        var result = text.replace(reg, function (match, p1, p2, p3) {
                            return p1 + "\\begin{bmatrix}" + p2 + "\\end{bmatrix}" + p3;
                        });
                        return result;
                    }
                },

                // hfill
                {
                    type: 'lang',
                    filter: function (text, converter, options) {
                        var language = options.language ? options.language : defaultLanguage;
                        taghfill = tags.hfill[language]
                        var reg = new RegExp(taghfill + '([\\S\\t ]*?)' + taghfill,'g');
                        var result = text.replace(reg, '<span class="hfill">$1</span><div class="clear"></div>');
                        return result;
                    }
                },

                // blank
                {
                    type: 'lang',
                    filter: function (text, converter, options) {
                        var reg = new RegExp('(\\.\\.)([\\S\\t ]+?)(\\.\\.)','g');
                        var result = text.replace(reg,'<span class="dot"></span><span class="blank">$2</span>');
                        return result;
                    }
                },

                // blankcirc
                {
                    type: 'lang',
                    filter: function (text, converter, options) {
                        var reg = new RegExp('(\\(\\()(.+?)(\\)\\))','g');
                        var result = text.replace(reg,'<span class="blankcirc"><span>$2</span></span>');
                        return result;
                    } 
                },

                // blankrec
                {
                    type: 'lang',
                    filter: function (text, converter, options) {
                        var reg = new RegExp('(\\[\\[)(.+?)(\\]\\])','g');
                        var result = text.replace(reg,'<span class="blankrec"><span>$2</span></span>');
                        return result;
                    } 
                },

                // // boom
                // {
                //     type: 'lang',
                //     filter: function (text, converter, options) {
                //         var reg = new RegExp('<boom>(.*?)<boom>');
                //         var result = text.replace(reg, ) 
                //     }
                // }

                // answer
                {
                    type: 'lang',
                    filter: function (text, converter, options) {
                        var reg = new RegExp('::(.+?)::');
                        var result = text.replace(reg, function(match, p1){
                            var answerBlock = '<span class="answer" >' + p1 + '</span>';
                            return answerBlock;
                        })
                        return result;
                    }
                },

                // parts
                {
                    type: 'lang',
                    filter: function (text, converter, options) {
                        var language = options.language ? options.language : defaultLanguage;
                        var tagParts = tags.parts[language];
                        var tagPart = tags.part[language];
                        var regParts = new RegExp('^[\\t ]*' + tagParts + '[ \\t]*(\\([\\S\\t ]*' + tagPart + ')?[ \\t]*(\n+[\\t ]*' + tagPart + '[ \\t]*[\\S\\t ]*\\S[\\t ]*(\n(?![\\t ]*' + tagPart + ')[\\S\\t ]*\\S[\\t ]*)*)+','gm');
                        var result = text.replace(regParts, function(match, p1){
                            var style = {};
                            if(p1) {
                                p1 = p1.substr(1,p1.length-2);
                                p1 = p1.split("|");
                                var regCol = new RegExp('^' + tags.cols[language] + '$')
                                var opt;
                                for (opt of p1) {
                                    opt = opt.trim().toEnDigit();
                                    var matches = opt.match(regCol)
                                    if(matches) {
                                        style['--cols'] = matches[1];
                                    }
                                }
                            }

                            var regPart = new RegExp('\n+[\\t ]*' + tagPart + '[ \\t]*([\\S\\t ]*\\S[\\t ]*(\n(?![\\t ]*' + tagPart + ')[\\S\\t ]*\\S[\\t ]*)*)','gm');
                            var partBlock;
                            var parts = "";
                            var items = match.matchAll(regPart);
                            var i = 0;
                            if(items) {
                                for(let item of items) {
                                    parts += '<div class="part"><span class="numbering">'+labelChar('part', i, options)+')</span>' + item[1] + '</div>';
                                    i++;
                                }
                            }
                            partBlock = ' <div class="parts" ' + renderStyle(style) + '>'+parts+'</div><div class="clear"></div>';
                            return partBlock;
                        });
                        return result;
                    }
                },

                // truefalse
                {
                    type: "lang",
                    filter: function (text, converter, options) {
                        var language = options.language ? options.language : defaultLanguage;
                        var tagProps = tags.props[language];
                        var tagProp = tags.prop[language];
                        var i;
                        var regProps = new RegExp('^[\\t ]*' + tagProps + '[ \\t]*(\\([\\S\\t ]*\\))?[ \\t]*(\n+[\\t ]*' + tagProp + '[ \\t]*[\\S\\t ]*\\S[\\t ]*(\n(?![\\t ]*\\))[\\S\\t ]*\\S[\\t ]*)*)+','gm');
                        var result = text.replace(regProps, function(match, p1){
                            var style = {};
                            if(p1) {
                                p1 = p1.substr(1,p1.length-2);
                                p1 = p1.split("|");
                                var regCol = new RegExp('^' + tags.cols[language] + '$')
                                var opt;
                                for (opt of p1) {
                                    opt = opt.trim().toEnDigit();
                                    var matches = opt.match(regCol)
                                    if(matches) {
                                        style['--cols'] = matches[1];
                                    }
                                }
                            }

                            var regProp = new RegExp('\n+[\\t ]*(' + tagProp + ')[ \\t]*([\\S\\t ]*\\S[\\t ]*(\n(?![\\t ]*' + tagProp + ')[\\S\\t ]*\\S[\\t ]*)*)','gm');
                            var propsBlock;
                            var props = "";
                            var items = match.matchAll(regProp);
                            var accuracy;
                            i = 0;
                            if(items) {
                                for(let item of items) {
                                    if (item[1] == 't)' || item[1] == 'د)') {
                                        accuracy = 'true';
                                    } else if (item[1] == 'f)' || item[1] == 'ن)') {
                                        accuracy = 'false';
                                    } else {
                                        accuracy = '';
                                    }
                                    props += '<div class="prop ' + accuracy + '"><span class="numbering">'+ labelChar('prop', i, options) + ')</span>' + '<span class="checkbox">' + checkbox + '</span><span>' + item[3] + '</span></div>';
                                    i++;
                                }
                            }
                            propsBlock = ' <div class="truefalse" ' + renderStyle(style) + '>' + props + '</div><div class="clear"></div>';
                            return propsBlock;
                        });
                        return result;
                    }
                },

                // multi choices
                {
                    type: "lang",
                    filter: function (text, converter, options) {
                        var language = options.language ? options.language : defaultLanguage;
                        var tagChoices = tags.choices[language];
                        var tagChoice = tags.choice[language];
                        var regChoices = new RegExp('^[\\t ]*' + tagChoices + '[ \\t]*(\\([\\S\\t ]*\\))?[ \\t]*(\n+[\\t ]*' + tagChoice + '[ \\t]*[\\S\\t ]*\\S[\\t ]*(\n(?![\\t ]*\\))[\\S\\t ]*\\S[\\t ]*)*)+','gm');
                        var result = text.replace(regChoices, function(match, p1){
                            var style = {};
                            if(p1) {
                                p1 = p1.substr(1,p1.length-2);
                                p1 = p1.split("|");
                                var regCol = new RegExp('^' + tags.cols[language] + '$')
                                var opt;
                                for (opt of p1) {
                                    opt = opt.trim().toEnDigit();
                                    var matches = opt.match(regCol)
                                    if(matches) {
                                        style['--cols'] = matches[1];
                                    }
                                }
                            }

                            var regChoice = new RegExp('\n+[\\t ]*(' + tagChoice + ')[ \\t]*([\\S\\t ]*\\S[\\t ]*(\n(?![\\t ]*' + tagChoice + ')[\\S\\t ]*\\S[\\t ]*)*)','gm');
                            var choicesBlock;
                            var choices = "";
                            var items = match.matchAll(regChoice);
                            var i = 0;
                            var accuracy;
                            if(items) {
                                for(let item of items) {
                                    if (item[1] == 't)' || item[1] == 'د)') {
                                        accuracy = 'true';
                                    } else if (item[1] == 'f)' || item[1] == 'ن)') {
                                        accuracy = 'false';
                                    } else {
                                        accuracy = '';
                                    }
                                    choices += '<div class="choice ' + accuracy + '"><span class="numbering">'+ labelChar('prop', i, options) + ')</span>' + '<span class="checkbox">' + checkbox + '</span><span>' + item[3] + '</span></div>';
                                    i++;
                                }
                            }
                            choicesBlock = ' <div class="choices" ' + renderStyle(style) + '>' + choices + '</div><div class="clear"></div>';
                            return choicesBlock;
                        });
                        return result;
                    }
                },

                // question
                {
                    type: "lang",
                    filter: function (text, converter, options) {
                        var language = options.language ? options.language : defaultLanguage;
                        var tagQues = tags.question[language];
                        var regQues = new RegExp('\n*^[\\t ]*' + tagQues + '[ \\t]*(\\(([\\S\\t ]*)\\))?[\\t ]*([\\S\\t ]*(\n(?![\\t ]*' + tagQues + ')[\\S\\t ]*\\S[\\t ]*)*)','gm');
                        var result = text.replace(regQues, function(match, p1, p2, p3, p4){
                            var d = converter.makeHtml(p3);
                            var quesBlock;
                            quesBlock = '<div class="question-group"><div class="number"></div><div class="question">' + d + '</div><div class="point">' + (p2 || '') +'</div></div>';
                            return quesBlock;
                        });
                        return result;
                    }
                }
            ]
        }
        showdown.extension('azmoon', azmoon);
}));