const PAPERSIZES = {
	'A4': {
		width: '210mm',
		height: '297mm'
	},
	'A5': {
		width: '148mm',
		height: '210mm'
	}
}

class Paper {
	width = '210mm';
	height = '297mm';
	constructor(size = 'A4', orientation = 'portrait') {
		this._setWidthHeight(PAPERSIZES[size].width, PAPERSIZES[size].height, orientation)
	}
	_setWidthHeight(width, height, orientation = 'portrait') {
		if (orientation === 'landscape') {
			this.width = height;
			this.height = width;
		} else {
			this.width = width;
			this.height = height;
		}
	}
}